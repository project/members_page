<?php
/**
 * @file
 * Code for the D7 Members Page module.
 */

include_once 'members.inc';

/**
 * Implements hook_help().
 */
function members_help($path, $arg) {
  if ($path == 'admin/help#members') {
    $page_link = variable_get('members_page_url', 'members');
    $out = <<< HELPTEXT

<div style="float:left;width:47%;padding:0 8px;border-right:solid grey 1px">

  <p>Drupal sites are subject to web-bot registrations that target Drupal's
  <b>/user/register</b> url. The Members Page module was crafted to fight this
  nuisance by creating a dedicated members page and a special user
  registration url. The latter is available even if the standard /user/register
  url is blocked. Both url's can be changed to make them unique.</p>

  <h4>The Members page</h4>

  <p>The <a href=/$page_link target="_blank" style="text-decoration:underline">
  Members page</a> has a two panel content block with three different viewing
  modes for the:
  <ol>
    <li><b>anonymous</b> visitor</li>
    <li><b>member</b> -- a user who has logged in by way of a session cookie or by
        providing a valid username and password</li>
    <li><b>admins</b> or <b>users</b> who have Member Page <i>edit permissions</i></li>
  </ol>

  <p>The visitor view has a greeting, a login block for existing members,
  and a link to a special registration form for new members.</p>

  <p>The members view has a greeting and a personalized configurable user-menu
  block.</p>

  <p>Both greetings can be edited - a good place to encourage sign-up of new
  members or to publish news items or new links for your current members.</p>

  <h4>Membership has its privileges</h4>

  <p>The Members Page registration form has elevated privileges. A visitor who completes
  this form will be registered immediately without admin approval (but still
  subject to email verification) <i>regardless of the current site settings</i>.</p>

  <p>The trick here is that the usual web-bots don't know where the url is for
  the members page or the special registration form. These url's can be
  changed by the administrator.</p>

</div><div style="float:left;width:47%;padding:0 8px">

  <h4>Features</h4>

  <p>A new fieldset has been added to the
  <a href="/admin/config/people/accounts" target="_blank" style="text-decoration:underline">
  Account Settings</a> page that allows a site administrator to specify the
  various Members Page options.</p>

  <p>The Members module creates a user registration form that enables immediate
  registration, as if the site settings were set to:</p>
  <p><pre>
  Registration and cancellation
     Who can register accounts?
       Visitors
  </pre></p>

  <h4>Altered links</h4>

  <p>The Members module alters the standard drupal links that point to the
  /user/register page and login page (/user). They point instead to the
  Members page. The Members module also combines the two Login and Register
  links into a single link labeled Add Comment or Reply, depending on the context.
  </p>

  <h4>Contributed module support</h4>

  <p>The Members Page module creates the alternate user register page using the resources
  of the standard drupal modules and certain contributed modules.  The
  contributed modules currently supported are:
  <ul>
    <li>LoginToboggan D6/D7
    <li>D6 Content Profile / D7 Profile 2 / D8 core
  </ul>

  <h4>Members Page menu link</h4>

  <p>A menu item (<i>Members</i>) can be found on the
  <a href="/admin/structure/menu/manage/main-menu" target="_blank">Main menu</a>.
  This menu link can be re-configured to appear wherever might be suitable.
  /p>

</div>
<hr style="clear:both"/><br />
  <h4>Trackback feature</h4>

  <p>This release has a <b>Comment trackback</b> feature that can be added to the
  verification email sent to a new member. Near the bottom of the configuration
  page is the <a href="/admin/config/people/accounts#edit-email-title">Welcome
  (no approval required)</a> link. Click on that to edit the
  email message. There is a token -- [user:one-time-login-url].  Append the
  [current-page:query:?] token to the first token and then edit as follows:</p>
  <pre>
  [user:one-time-login-url]?destination=[current-page:query:destination]
  </pre>
  <p>When a user clicks on the link in the email verification that is sent, the
  notification and password request work as before; but after the password has
  been submitted (and any other required info), the visitor is returned to
  the comment form on the page that triggered the Add Comment/Reply action.</p>

<hr style="clear:both"/>
HELPTEXT;
    return $out;
  }
}

/**
 * Implements hook_menu().
 */
function members_menu() {
  // There are two configurable urls in this module.
  $register_path = variable_get('members_register_url', 'register/user');
  $members_path = variable_get('members_page_url', 'members');

  $items[$members_path] = array(
    'title' => 'Members',
    'description' => 'Login or register a username.',
    'page callback' => 'members_page',
    'access callback' => TRUE,
    'menu_name' => 'main-menu',
    'type' => MENU_NORMAL_ITEM,
  );

  $items[$register_path] = array(
    'title' => 'Create new account',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('members_user_register_form'),
    'access callback' => 'user_is_anonymous',
    'type' => MENU_CALLBACK,
  );

  $items[$members_path . '/view'] = array(
    'title' => 'View',
    'description' => 'Login or register a username.',
    'page callback' => 'members_page',
    'access arguments' => array('edit member page greeting'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => 5,
  );

  $items[$members_path . '/edit/member'] = array(
    'title' => 'Member',
    'description' => 'Edit the greetings for the members page',
    'page callback' => 'members_render_member_page',
    'access arguments' => array('edit member page greeting'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 10,
  );

  $items[$members_path . '/edit/anon'] = array(
    'title' => 'Visitor',
    'description' => 'Edit the greetings for the members page',
    'page callback' => 'members_render_visitor_page',
    'access arguments' => array('edit member page greeting'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 15,
  );
  return $items;
}

/**
 * Implements hook_permission().
 */
function members_permission() {
  return array(
    'edit member page greeting' => array(
      'title' => t('Edit the Member page greetings'),
      'description' => t('Edit the greetings for each view mode of the Members page.'),
    ),
  );
}

/**
 * Helper function to return the left/right panel widths of the enclosing* divs.
 *
 * @returns an array for the left and right panel widths in % units.
 * @see members_editMemberGreeting()
 * @see members_editAnonGreeting()
 */
function members_panel_width() {
  $width = variable_get('members_panel_width', 30);
  $right = 'width:' . $width . '%';
  $val = round((1.0 - ($width / 100)) * 100 - 0.5, 1);
  $left = 'width:' . $val . '%';
  $panel_width = array(
    'left'  => $left,
    'right' => $right,
  );
  return $panel_width;
}

/**
 * Implements hook_theme().
 */
function members_theme() {
  $theme = array(
    'members-page' => array(
      'type' => 'module',
      'path' => drupal_get_path('module', 'members'),
      'template' => 'members-page',
      'variables' => array(
        'greeting' => array('#markup' => 'left panel'),
        'sidebar' => array('#markup' => 'right panel'),
        'panel_width' => array('left' => '70%', 'right' => '30%'),
        'register' => ' a link to the special user registration page',
      ),
    ),
  );

  return $theme;
}

/**
 * Return a form to edit the greeting to members on the Members page.
 */
function members_greeting_form($form, &$form_state) {
  $greeting = variable_get('members_greeting_authenticated');

  $form['member'] = array(
    '#type' => 'text_format',
    '#rows' => 8,
    '#cols' => 60,
    '#title' => t('Edit the greeting for members who have logged in.'),
    '#description' => t('This greeting will appear on the Members page for members who are logged in by cookie or with the correct username/password.'),
    '#default_value' => $greeting['value'],
    '#format' => $greeting['format'],
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );
  return $form;
}

/**
 * Return a form to edit the greeting to anon visitors on the Members page.
 */
function members_greeting_anon_form($form, &$form_state) {
  $greeting = variable_get('members_greeting_anonymous');
  $width = members_panel_width();

  $form['visitor'] = array(
    '#type' => 'text_format',
    '#rows' => 8,
    '#cols' => 60,
    '#title' => t('Edit the greeting for anonymous visitors.'),
    '#description' => t('This greeting will appear on the Members page for anonymous visitors or members who have logged out.'),
    '#default_value' => $greeting['value'],
    '#format' => $greeting['format'],
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );
  return $form;
}

/**
 * Callback for members_editAnonGreeting().
 */
function members_greeting_anon_form_submit($form, &$form_state) {
  variable_set('members_greeting_anonymous', filter_xss_admin($form_state['values']['visitor']));
  drupal_goto('/' . variable_get('members_page_url'), array('members'));
}

/**
 * Callback for members_editMemberGreeting().
 */
function members_greeting_form_submit($form, &$form_state) {
  variable_set('members_greeting_authenticated', filter_xss_admin($form_state['values']['member']));
  drupal_goto('/' . variable_get('members_page_url'), array('members'));
}

/**
 * Initial greetings for the members page.
 *
 * @see members_page()
 */
function members_greeting() {
  $info = array(
    '#prefix' => '<div class="node">',
    '#suffix' => '</div>',
    '#type' => 'text_format',
  );

  if (user_is_anonymous()) {
    $greeting = variable_get('members_greeting_anonymous');
    $info['#children'] = check_markup($greeting['value'], $greeting['format']);
  } else {
    $greeting = variable_get('members_greeting_authenticated');
    $info['#children'] = check_markup($greeting['value'], $greeting['format']);
  }
  return $info;
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * This hook is used to pass a destination url provided by the use of the
 * [current-page:query:?] token in the 'no approval required - welcome email
 * message' found on the /admin/config/people/accounts page.
 */
function members_form_user_pass_reset_alter(&$form, &$form_state, $form_id) {
  if (isset($_GET['destination'])) {
    // We attach a 'caller' query attribute here instead of 'destination'
    // because using 'destination' resulted in a premature redirect to the
    // destination page.
    $str = drupal_encode_path($_GET['destination']);
    $form['#action'] .= '?caller=' . $str;

    // Now we set a $_SESSION value so that it can be retrieved when a visitor
    // who has just created a new account and set his password can be returned
    // original page to post a comment.  At this point, a $_SESSION object may
    // not exist yet since it is an anonymous visitor.  This will cause an error
    // message because D7 does not check before session_start() is invoked.
    if (!isset($_SESSION)) {
      session_start();
    }
    $_SESSION['members_page_recall'] = $_GET['destination'];
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * This hook is used to retrieve the original destination page from the
 * $_SESSION cookie. This value is then added to the query that invokes the
 * user profile edit page. After entering the desired password values, the user
 * is returned to the page where they first clicked on the 'Reply' or 'Add
 * comment' link.
 */
function members_form_user_profile_form_alter(&$form, &$form_state, $form_id) {
  if (isset($_GET['pass-reset-token']) && isset($_SESSION['members_page_recall'])) {
    $str = drupal_encode_path($_SESSION['members_page_recall']);
    $form['#action'] .= '&destination=' . $str;
    unset($_SESSION['members_page_recall']);
  }
  // A Drupal error message will be generated from:
  // members_form_user_pass_reset_alter()
  // if session_start() has NOT been invoked. The next few lines unsets this
  // error message.
  if (isset($_SESSION['messages']['error'])) {
    foreach ($_SESSION['messages']['error'] as $key => $val) {
      if (preg_match('/A session had already been started - ignoring session_start/', $val)) {
        unset($_SESSION['messages']['error'][$key]);
      }
      // If there are no errors left, unset ther error messages.
      if (empty($_SESSION['messages']['error'])) {
        unset($_SESSION['messages']['error']);
      }
    }
  }
}

/**
 * Renders the 'members' page.
 *
 * The members page serves as a central point for:
 *  -Site news for returning visitors
 *  -user login/logout
 *  -A link to create a new account for new visitors (elevated privileges)
 */
function members_page() {
  drupal_add_css(drupal_get_path('module', 'members') . '/members-page.css');

  $sidebar = user_is_anonymous() ? members_render_login_block() : members_render_usermenu_block();
  $register = '';
  if (user_is_anonymous()) {
    // Create a link to the privileged register page.
    $options = array(
      'attributes' => array('id' => 'memberslink'),
      'query' => drupal_get_destination(),
    );
    $register = l(t('Register now!'), variable_get('members_register_url', 'register/user'), $options);
  }

  $variables = array(
    'greeting' => members_greeting(),
    'sidebar' => $sidebar,
    'panel_width' => members_panel_width(),
    'register' => $register,
  );
  $out = theme('members-page', $variables);
  return $out;
}

/**
 * Renders the admin form for editing the greeting for authenticated members.
 *
 * @return html
 *   html from the members-page.tpl.php template.
 */
function members_render_member_page() {
  drupal_add_css(drupal_get_path('module', 'members') . '/members-page.css');

  $variables = array(
    'greeting' => drupal_get_form('members_greeting_form'),
    'sidebar' => members_render_usermenu_block(),
    'panel_width' => members_panel_width(),
    'register' => '',
  );

  $out = theme('members-page', $variables);
  return $out;
}

/**
 * Renders the admin form for editing the greeting for anonymous visitors.
 *
 * @return html
 *   html from the members-page.tpl.php template.
 */
function members_render_visitor_page() {
  drupal_add_css(drupal_get_path('module', 'members') . '/members-page.css');

  $options = array('attributes' => array('id' => 'members_link'));
  $register = l(t('Register now!'), variable_get('members_register_url', 'register/user'), $options);
  $variables = array(
    'greeting' => drupal_get_form('members_greeting_anon_form'),
    'sidebar' => members_render_bogus_block(),
    'panel_width' => members_panel_width(),
    'register' => $register,
  );

  $out = theme('members-page', $variables);
  return $out;
}

/**
 * Renders a user menu block using function calls.
 */
function members_render_usermenu_block() {
  $fields = array(
    'module' => 'system',
    'delta' => 'user-menu',
    'region' => 'content',
    'title' => NULL,
    'cache' => -1,
  );
  $block = members_render_magic_block($fields);
  return '<div id="members-menu" class="sidebar">' . drupal_render($block) . '</div>';
}

/**
 * Renders a user login block using function calls.
 */
function members_render_login_block() {
  $fields = array(
    'module' => 'user',
    'delta' => 'login',
    'region' => 'content',
    'title' => NULL,
    'cache' => -1,
  );
  $block = members_render_magic_block($fields);
  return '<div id="members-login" class="sidebar">' . drupal_render($block) . '</div>';
}

/**
 * Renders a disabled user login block for display to an authenticated user.
 *
 * @see members_block_view()
 */
function members_render_bogus_block() {
  $fields = array(
    'module' => 'members',
    'delta' => 'login',
    'region' => 'content',
    'title' => NULL,
    'cache' => -1,
  );
  $block = members_render_magic_block($fields);
  return '<div id="members-bogus" class="sidebar">' . drupal_render($block) . '</div>';
}

/**
 * A disabled login form for display purposes when the user is authenticated.
 *
 * @see members_block_view()
 */
function members_bogus_login_form($form) {
  // Get the standard form.
  $bogus = user_login_block($form);

  // Make the form inoperable.
  $bogus['#id'] = 'bogus-user-login-form';
  unset($bogus['#action']);
  unset($bogus['#validate']);
  unset($bogus['#submit']);

  $bogus['name']['#disabled'] = TRUE;
  $bogus['pass']['#disabled'] = TRUE;
  $bogus['actions']['submit']['#disabled'] = TRUE;

  return $bogus;
}

/**
 * Implements hook_block_view() for members_bogus_login_form().
 *
 * @see members_bogus_login_form($form)
 */
function members_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case 'login':
      $block['subject'] = t('User login (disabled)');
      $block['content'] = drupal_get_form('members_bogus_login_form');
      return $block;
  }
}

/**
 * Callback for members_menu().
 *
 * Note that D7 & D6 versions differ here
 * @see members_menu()
 */
function members_user_register_form($form, &$form_state) {
  // Get the old value and temporarily set the new variable.
  $old = variable_get('user_register');
  variable_set('user_register', USER_REGISTER_VISITORS);

  // Generate the user_register_form for immediate login.
  $form = user_register_form($form, $form_state);

  // Code for compatibility with other contributed modules.
  if (module_exists('logintoboggan')) {
    logintoboggan_form_user_register_form_alter($form, $form_state);

    // We have to replace the logintoboggan submit with our own version.
    foreach ($form['#submit'] as $key => $callback) {
      if ($callback == 'logintoboggan_user_register_submit') {
        $form['#submit'][$key] = 'members_logintoboggan_user_register_submit';
      }
    }
  }
  if (module_exists('profile2')) {
    profile2_form_user_register_form_alter($form, $form_state);
  }

  // There are two submit functions needed, the first to preprocess the
  // $_GET['destination'] for the email; the second to set it back to what
  // it was so the member_user_register_form returns to the proper destination.
  $form['#submit'] = array_merge(array('members_user_register_form_first_submit'), $form['#submit']);
  $form['#submit'][] = 'members_user_register_form_last_submit';

  variable_set('user_register', $old);

  return $form;
}

/*
 * The first submit function for the members_user_register form.
 *
 * This function does a pseudo urlencode() to the # or 'fragment' portion
 * of the destination. This is done so the ID (i.e. id="comment-form") can be
 * passed along to the next step in the process.
 *
 */
function members_user_register_form_first_submit($form, &$form_state) {
  if (isset($_GET['destination'])) {
    $_GET['destination'] = drupal_encode_path($_GET['destination']);
  }
}

/*
 * The last submit function for the members_user_register form.
 *
 * This function flips a %23 back to a # character since subsequent
 * processing will do that for us.
 */
function members_user_register_form_last_submit($form, &$form_state) {
  if (isset($_GET['destination'])) {
    $_GET['destination'] = rawurldecode($_GET['destination']);
  }
}

/**
 * Members submit handler for LoginToboggan module.
 *
 * The LoginToboggan submit handler references the 'user_register' variable.
 * This function sets and resets 'user_register' for LoginToboggan.
 */
function members_logintoboggan_user_register_submit($form, &$form_state) {
  // Get the old value and temporarily set the new variable.
  $old = variable_get('user_register');
  variable_set('user_register', USER_REGISTER_VISITORS);
  logintoboggan_user_register_submit($form, $form_state);
  // Reset to the original value.
  variable_set('user_register', $old);
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Replaces the standard link to user/register on the user_login_block page
 * with the current Members url.
 */
function members_form_user_login_block_alter(&$form, &$form_state, $form_id) {
  unset($form['links']);
  $items = array();
  $items[] = l(t('Request new password'), 'user/password', array('attributes' => array('title' => t('Request new password via e-mail.'))));
  $form['links'] = array('#markup' => theme('item_list', array('items' => $items)));
}

/**
 * Implements hook_menu_alter().
 *
 * Removes the tabbed menu links  to user/register and user/login to prevent
 * all viewers from accessing the "create new account" page.
 */
function members_menu_alter(&$items) {
  $items['user/login']['type'] = MENU_CALLBACK;
  $items['user/register']['type'] = MENU_CALLBACK;
  $items['user/password']['type'] = MENU_CALLBACK;

  // Alter the Logout link for the members page.
  $items['user/logout']['page callback'] = 'members_logout';
  $items['user/logout']['module'] = 'members';
  if (isset($items['user/logout']['file'])) {
    unset($items['user/logout']['file']);
  }
}

/**
 * This module logout function returns the user to the members page.
 */
function members_logout() {
  global $user;

  watchdog('user', 'Session closed for %name.', array('%name' => $user->name));
  module_invoke_all('user_logout', $user);

  // Destroy the current session, and reset $user to the anonymous user.
  session_destroy();

  // The only thing that is different here from standard.
  drupal_goto(variable_get('members_page_url', 'members'));
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Adds a textfield for a url that points to a user registration page with
 * elevated privileges (no admin approval required). Alters the
 * user_admin_settings form.
 *
 * @see user_admin_settings()
 * @see members_settings_validate()
 * @see members_settings_submit()
 */
function members_form_user_admin_settings_alter(&$form, &$form_state, $form_id) {
  global $base_url;

  // We add a new element to the user_admin_settings form.
  $form['registration_cancellation']['members'] = array(
    '#type' => 'fieldset',
    '#title' => t('Members Page configuration'),
    '#collapsible' => TRUE,
    '#weight' => 0,
  );

  $form['registration_cancellation']['members']['members_register_url'] = array(
    '#type' => 'textfield',
    '#title' => t('<i>Members Page</i> registration url'),
    '#field_prefix' => $base_url . '/',
    '#default_value' => variable_get('members_register_url', 'register/user'),
    '#description' => t('Users who register on this page will not need administrator approval.'),
    '#required' => TRUE,
    '#size' => 20,
    '#maxlength' => 64,
    '#weight' => 10,
    '#prefix' => '<div style="float:left;clear:left;padding-right:30px;border:none">',
    '#suffix' => '</div>',
  );

  $form['registration_cancellation']['members']['members_page_url'] = array(
    '#type' => 'textfield',
    '#title' => t('<i>Members Page</i> url'),
    '#field_prefix' => $base_url . '/',
    '#default_value' => variable_get('members_page_url', 'members'),
    '#description' => t('Comment and reply links for anonymous visitors will point to this page.'),
    '#required' => TRUE,
    '#size' => 20,
    '#maxlength' => 64,
    '#weight' => 0,
    '#prefix' => '<div style="float:left;clear:left;padding-right:30px;border:none">',
    '#suffix' => '</div>',
  );

  $form['registration_cancellation']['members']['members_links'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Members Page links'),
    '#default_value' => variable_get('members_links', TRUE),
    '#description' => '<em>Login</em> and <em>Register</em> links are replaced with links to the <strong>Members</strong> page.',
    '#weight' => 20,
  );

  $form['registration_cancellation']['members']['members_panel_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width %'),
    '#default_value' => variable_get('members_panel_width', 30),
    '#description' => t('The width of the right panel on the Members Page.'),
    '#size' => 2,
    '#maxlength' => 2,
    '#weight' => 5,
    '#prefix' => '<div style="float:left;clear:right;padding-right:8px;border:none">',
    '#suffix' => '</div><br style="clear:both" />',
  );

  // Add validate and submit functions (callbacks) for the new element.
  $form['#validate'][] = 'members_settings_validate';
  $form['#submit'][] = 'members_settings_submit';
}

/**
 * Validates the members url.
 *
 * Callback added to user_admin_settings()
 * @see members_form_user_admin_settings_alter()
 */
function members_settings_validate($form, &$form_state) {
  global $base_url;

  $old_url = variable_get('members_register_url', 'register/user');
  $new_url = filter_xss_admin($form_state['values']['members_register_url']);
  if ($new_url != $old_url) {
    $path = $base_url . '/' . $new_url;
    drupal_set_message(t("checking for @path", array('@path' => $path)));
    if (members_check_url_free($new_url) == FALSE) {
      form_set_error('', 'Requested url is already in use, please specify another.');
    }
  }

  $old_url = variable_get('members_page_url', 'members');
  $new_url = filter_xss_admin($form_state['values']['members_page_url']);
  if ($new_url != $old_url) {
    $path = $base_url . '/' . $new_url;
    drupal_set_message(t("checking for @path", array('@path' => $path)));
    if (members_check_url_free($new_url) == FALSE) {
      form_set_error('', 'Requested url is already in use, please specify another.');
    }
  }

  $width = $form_state['values']['members_panel_width'];
  if ($width < 10 || $width > 90) {
    form_set_error('', 'Members Page panel width needs to be between 10% and 90%');
  }
}

/**
 * Submits the members settings for processing.
 *
 * Surprise - the variables that are instantiated and updated by this module
 * never need to be actually set with variable_set(). It appears that they are
 * set by an appearance as a field on an admin form.
 *
 * @see members_form_user_admin_settings_alter()
 */
function members_settings_submit($form, &$form_state) {
  // @todo - 'Members' menu item is disabled on a url change - not sure why.
  menu_rebuild();
}

/**
 * Implements hook_block_view_alter().
 *
 * Changes the block title on the User menu and Navigation block D6 already had
 * this feature, this code adds it to D7.
 */
function members_block_view_alter(&$data, $block) {
  global $user;

  if ($block->delta == 'user-menu' || $block->delta == 'navigation') {
    $uid = $user->uid;
    $account = array('account' => user_load($uid));
    $data['subject'] = isset($user->name) ? theme('username', $account) : $data['subject'];
  }
}

/**
 * Implements hook_node_view_alter().
 *
 * Removes the 'Add comment' link on a teaser.  It adds clutter, and a visitor
 * should NOT be allowed to add a comment from a teaser. It's an invitation
 * for trolls.
 * @see comment_node_view()
 */
function members_node_view_alter(&$build) {
  if (variable_get('members_links', TRUE)) {
    if (isset($build['links']['comment']['#links']['comment-add'])) {
      unset($build['links']['comment']['#links']['comment-add']);
    }
  }
}

/**
 * Implements hook_node_view().
 *
 * If a node allows comments, then drupal creates a destination link to the
 * comment-form so that a user who has not logged in can log in and return
 * to comment.  This function re-writes those links for the Members module.
 */
function members_node_view($node, $view_mode, $langcode) {
  if (variable_get('members_links', TRUE)) {
    if ($view_mode == 'teaser') {
      if (isset($node->content['links']['comment']['#links']['comment_forbidden'])) {
        unset($node->content['links']['comment']['#links']['comment_forbidden']);
      }
    }
    // Assumes an anon visitor AND that anon's cannot post comments.
    if (isset($node->content['links']['comment']['#links']['comment_forbidden']['title'])) {
      // Important 'alias' must be TRUE for the Trackback to work properly.
      // The #comment-form is the standard target.
      $dest = url('node/' . $node->nid, array('fragment' => 'edit-comment-body-und-0-value', 'alias' => TRUE));
      // The #scayt_0 is for a ckeditor implementation.
      // The url function adds a leading slash here -- remove it.
      if (substr($dest, 0, 1) == '/') {
        $dest = substr($dest, 1);
      }

      $destination = array('destination' => $dest);
      $url = variable_get('members_page_url', 'members');
      $members = url($url, array('query' => $destination));
      $html = t('<a href="@members">Add comment</a>', array('@members' => $members));
      $node->content['links']['comment']['#links']['comment_forbidden'] = array(
        'title' => $html,
        'html' => TRUE,
      );
    }
  } else {
    // Simple string substitution.
    $html = $node->content['links']['comment']['#links']['comment_forbidden']['title'];
    $html = str_replace('href="/user/login', 'href="/' . variable_get('members_page_url', 'members'), $html);
    $html = str_replace('href="/user/register', 'href="/' . variable_get('members_register_url', 'register/user'), $html);
    $node->content['links']['comment']['#links']['comment_forbidden']['title'] = $html;
  }
}

/**
 * Implements hook_comment_view().
 *
 * If an anon user wants to comment or reply to a comment, drupal creates a
 * link to the login and user/register page that allows the viewer to login
 * and return to the same page to comment or reply to a comment. This function
 * re-writes those links for the Members module.
 */
function members_comment_view($comment, $view_mode, $langcode) {
  // Assumes an anon visitor AND that anon's cannot post comments.
  if (isset($comment->content['links']['comment']['#links']['comment_forbidden'])) {
    $destination = array('destination' => "comment/reply/$comment->nid/$comment->cid");
    $url = variable_get('members_page_url', 'members');
    $members = url($url, array('query' => $destination));
    $html = t('<a href="@members">Reply</a>', array('@members' => $members));
    $comment->content['links']['comment']['#links']['comment_forbidden'] = array(
      'title' => $html,
      'html' => TRUE,
    );
  }
}
